package com.example.myapplication

import com.example.myapplication.extestion.roundUpTo
import com.example.myapplication.model.FxItem
import org.junit.Assert
import org.junit.Test

class ExtensionTester {
    @Test
    fun testRoundUpExtension(){
        val result = "0.02" == 0.025.roundUpTo(2)
        Assert.assertEquals(false, result)
    }
}