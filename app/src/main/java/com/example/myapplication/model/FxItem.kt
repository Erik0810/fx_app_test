package com.example.myapplication.model

class FxItem(val name: String, var value: Double) {
    val sell: Double?
    val buy: Double?
    var change: Double = 0.0
    var balance = 10000.0

    init {
        sell = randomEditValue(value)
        buy = randomEditValue(value)
        value = randomEditValue(value)
    }

    private fun randomEditValue(value: Double): Double {
        val random = (-10..10).random()
        return value + (random * 0.0001)
    }
}