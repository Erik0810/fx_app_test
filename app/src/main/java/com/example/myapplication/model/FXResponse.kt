package com.example.myapplication.model

import android.util.Log
import com.example.myapplication.extestion.toJsonString
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import java.lang.reflect.Field

data class FXResponse(

    @field:SerializedName("terms")
    val terms: String? = null,

    @field:SerializedName("success")
    val success: Boolean? = null,

    @field:SerializedName("privacy")
    val privacy: String? = null,

    @field:SerializedName("source")
    val source: String? = null,

    @field:SerializedName("timestamp")
    val timestamp: Int? = null,

    @field:SerializedName("quotes")
    val quotes: Quotes? = null
)

class Quotes(

    @field:SerializedName("USDBIF")
    var uSDBIF: Double? = null,

    @field:SerializedName("USDMWK")
    var uSDMWK: Double? = null,

    @field:SerializedName("USDBYR")
    var uSDBYR: Int? = null,

    @field:SerializedName("USDBYN")
    var uSDBYN: Double? = null,

    @field:SerializedName("USDHUF")
    var uSDHUF: Double? = null,

    @field:SerializedName("USDAOA")
    var uSDAOA: Double? = null,

    @field:SerializedName("USDJPY")
    var uSDJPY: Double? = null,

    @field:SerializedName("USDMNT")
    var uSDMNT: Double? = null,

    @field:SerializedName("USDPLN")
    var uSDPLN: Double? = null,

    @field:SerializedName("USDGBP")
    var uSDGBP: Double? = null,

    @field:SerializedName("USDSBD")
    var uSDSBD: Double? = null,

    @field:SerializedName("USDAWG")
    var uSDAWG: Double? = null,

    @field:SerializedName("USDKRW")
    var uSDKRW: Double? = null,

    @field:SerializedName("USDNPR")
    var uSDNPR: Double? = null,

    @field:SerializedName("USDINR")
    var uSDINR: Double? = null,

    @field:SerializedName("USDYER")
    var uSDYER: Double? = null,

    @field:SerializedName("USDAFN")
    var uSDAFN: Double? = null,

    @field:SerializedName("USDMVR")
    var uSDMVR: Double? = null,

    @field:SerializedName("USDKZT")
    var uSDKZT: Double? = null,

    @field:SerializedName("USDSRD")
    var uSDSRD: Double? = null,

    @field:SerializedName("USDSZL")
    var uSDSZL: Double? = null,

    @field:SerializedName("USDLTL")
    var uSDLTL: Double? = null,

    @field:SerializedName("USDSAR")
    var uSDSAR: Double? = null,

    @field:SerializedName("USDTTD")
    var uSDTTD: Double? = null,

    @field:SerializedName("USDBHD")
    var uSDBHD: Double? = null,

    @field:SerializedName("USDHTG")
    var uSDHTG: Double? = null,

    @field:SerializedName("USDANG")
    var uSDANG: Double? = null,

    @field:SerializedName("USDPKR")
    var uSDPKR: Double? = null,

    @field:SerializedName("USDXCD")
    var uSDXCD: Double? = null,

    @field:SerializedName("USDLKR")
    var uSDLKR: Double? = null,

    @field:SerializedName("USDNGN")
    var uSDNGN: Double? = null,

    @field:SerializedName("USDCRC")
    var uSDCRC: Double? = null,

    @field:SerializedName("USDCZK")
    var uSDCZK: Double? = null,

    @field:SerializedName("USDZWL")
    var uSDZWL: Double? = null,

    @field:SerializedName("USDGIP")
    var uSDGIP: Double? = null,

    @field:SerializedName("USDRON")
    var uSDRON: Double? = null,

    @field:SerializedName("USDMMK")
    var uSDMMK: Double? = null,

    @field:SerializedName("USDMUR")
    var uSDMUR: Double? = null,

    @field:SerializedName("USDNOK")
    var uSDNOK: Double? = null,

    @field:SerializedName("USDSYP")
    var uSDSYP: Double? = null,

    @field:SerializedName("USDIMP")
    var uSDIMP: Double? = null,

    @field:SerializedName("USDCAD")
    var uSDCAD: Double? = null,

    @field:SerializedName("USDBGN")
    var uSDBGN: Double? = null,

    @field:SerializedName("USDRSD")
    var uSDRSD: Double? = null,

    @field:SerializedName("USDDOP")
    var uSDDOP: Double? = null,

    @field:SerializedName("USDKMF")
    var uSDKMF: Double? = null,

    @field:SerializedName("USDCUP")
    var uSDCUP: Double? = null,

    @field:SerializedName("USDGMD")
    var uSDGMD: Double? = null,

    @field:SerializedName("USDTWD")
    var uSDTWD: Double? = null,

    @field:SerializedName("USDIQD")
    var uSDIQD: Double? = null,

    @field:SerializedName("USDSDG")
    var uSDSDG: Double? = null,

    @field:SerializedName("USDBSD")
    var uSDBSD: Double? = null,

    @field:SerializedName("USDSLL")
    var uSDSLL: Double? = null,

    @field:SerializedName("USDCUC")
    var uSDCUC: Int? = null,

    @field:SerializedName("USDZAR")
    var uSDZAR: Double? = null,

    @field:SerializedName("USDTND")
    var uSDTND: Double? = null,

    @field:SerializedName("USDCLP")
    var uSDCLP: Double? = null,

    @field:SerializedName("USDHNL")
    var uSDHNL: Double? = null,

    @field:SerializedName("USDUGX")
    var uSDUGX: Double? = null,

    @field:SerializedName("USDMXN")
    var uSDMXN: Double? = null,

    @field:SerializedName("USDSTD")
    var uSDSTD: Double? = null,

    @field:SerializedName("USDLVL")
    var uSDLVL: Double? = null,

    @field:SerializedName("USDSCR")
    var uSDSCR: Double? = null,

    @field:SerializedName("USDCDF")
    var uSDCDF: Double? = null,

    @field:SerializedName("USDBBD")
    var uSDBBD: Double? = null,

    @field:SerializedName("USDGTQ")
    var uSDGTQ: Double? = null,

    @field:SerializedName("USDFJD")
    var uSDFJD: Double? = null,

    @field:SerializedName("USDTMT")
    var uSDTMT: Double? = null,

    @field:SerializedName("USDCLF")
    var uSDCLF: Double? = null,

    @field:SerializedName("USDBRL")
    var uSDBRL: Double? = null,

    @field:SerializedName("USDPEN")
    var uSDPEN: Double? = null,

    @field:SerializedName("USDNZD")
    var uSDNZD: Double? = null,

    @field:SerializedName("USDWST")
    var uSDWST: Double? = null,

    @field:SerializedName("USDNIO")
    var uSDNIO: Double? = null,

    @field:SerializedName("USDBAM")
    var uSDBAM: Double? = null,

    @field:SerializedName("USDEGP")
    var uSDEGP: Double? = null,

    @field:SerializedName("USDMOP")
    var uSDMOP: Double? = null,

    @field:SerializedName("USDNAD")
    var uSDNAD: Double? = null,

    @field:SerializedName("USDBZD")
    var uSDBZD: Double? = null,

    @field:SerializedName("USDMGA")
    var uSDMGA: Double? = null,

    @field:SerializedName("USDXDR")
    var uSDXDR: Double? = null,

    @field:SerializedName("USDCOP")
    var uSDCOP: Double? = null,

    @field:SerializedName("USDRUB")
    var uSDRUB: Double? = null,

    @field:SerializedName("USDPYG")
    var uSDPYG: Double? = null,

    @field:SerializedName("USDISK")
    var uSDISK: Double? = null,

    @field:SerializedName("USDJMD")
    var uSDJMD: Double? = null,

    @field:SerializedName("USDLYD")
    var uSDLYD: Double? = null,

    @field:SerializedName("USDBMD")
    var uSDBMD: Int? = null,

    @field:SerializedName("USDKWD")
    var uSDKWD: Double? = null,

    @field:SerializedName("USDPHP")
    var uSDPHP: Double? = null,

    @field:SerializedName("USDBDT")
    var uSDBDT: Double? = null,

    @field:SerializedName("USDCNY")
    var uSDCNY: Double? = null,

    @field:SerializedName("USDTHB")
    var uSDTHB: Double? = null,

    @field:SerializedName("USDUZS")
    var uSDUZS: Double? = null,

    @field:SerializedName("USDXPF")
    var uSDXPF: Double? = null,

    @field:SerializedName("USDMRO")
    var uSDMRO: Double? = null,

    @field:SerializedName("USDIRR")
    var uSDIRR: Double? = null,

    @field:SerializedName("USDARS")
    var uSDARS: Double? = null,

    @field:SerializedName("USDQAR")
    var uSDQAR: Double? = null,

    @field:SerializedName("USDGNF")
    var uSDGNF: Double? = null,

    @field:SerializedName("USDERN")
    var uSDERN: Double? = null,

    @field:SerializedName("USDMZN")
    var uSDMZN: Double? = null,

    @field:SerializedName("USDSVC")
    var uSDSVC: Double? = null,

    @field:SerializedName("USDBTN")
    var uSDBTN: Double? = null,

    @field:SerializedName("USDUAH")
    var uSDUAH: Double? = null,

    @field:SerializedName("USDKES")
    var uSDKES: Double? = null,

    @field:SerializedName("USDSEK")
    var uSDSEK: Double? = null,

    @field:SerializedName("USDCVE")
    var uSDCVE: Double? = null,

    @field:SerializedName("USDAZN")
    var uSDAZN: Double? = null,

    @field:SerializedName("USDTOP")
    var uSDTOP: Double? = null,

    @field:SerializedName("USDOMR")
    var uSDOMR: Double? = null,

    @field:SerializedName("USDPGK")
    var uSDPGK: Double? = null,

    @field:SerializedName("USDXOF")
    var uSDXOF: Double? = null,

    @field:SerializedName("USDGEL")
    var uSDGEL: Double? = null,

    @field:SerializedName("USDBTC")
    var uSDBTC: Double? = null,

    @field:SerializedName("USDUYU")
    var uSDUYU: Double? = null,

    @field:SerializedName("USDMAD")
    var uSDMAD: Double? = null,

    @field:SerializedName("USDFKP")
    var uSDFKP: Double? = null,

    @field:SerializedName("USDMYR")
    var uSDMYR: Double? = null,

    @field:SerializedName("USDEUR")
    var uSDEUR: Double? = null,

    @field:SerializedName("USDLSL")
    var uSDLSL: Double? = null,

    @field:SerializedName("USDDKK")
    var uSDDKK: Double? = null,

    @field:SerializedName("USDJOD")
    var uSDJOD: Double? = null,

    @field:SerializedName("USDHKD")
    var uSDHKD: Double? = null,

    @field:SerializedName("USDRWF")
    var uSDRWF: Double? = null,

    @field:SerializedName("USDAED")
    var uSDAED: Double? = null,

    @field:SerializedName("USDBWP")
    var uSDBWP: Double? = null,

    @field:SerializedName("USDSHP")
    var uSDSHP: Double? = null,

    @field:SerializedName("USDTRY")
    var uSDTRY: Double? = null,

    @field:SerializedName("USDLBP")
    var uSDLBP: Double? = null,

    @field:SerializedName("USDTJS")
    var uSDTJS: Double? = null,

    @field:SerializedName("USDIDR")
    var uSDIDR: Double? = null,

    @field:SerializedName("USDKYD")
    var uSDKYD: Double? = null,

    @field:SerializedName("USDAMD")
    var uSDAMD: Double? = null,

    @field:SerializedName("USDGHS")
    var uSDGHS: Double? = null,

    @field:SerializedName("USDGYD")
    var uSDGYD: Double? = null,

    @field:SerializedName("USDKPW")
    var uSDKPW: Double? = null,

    @field:SerializedName("USDBOB")
    var uSDBOB: Double? = null,

    @field:SerializedName("USDKHR")
    var uSDKHR: Double? = null,

    @field:SerializedName("USDMDL")
    var uSDMDL: Double? = null,

    @field:SerializedName("USDAUD")
    var uSDAUD: Double? = null,

    @field:SerializedName("USDILS")
    var uSDILS: Double? = null,

    @field:SerializedName("USDTZS")
    var uSDTZS: Double? = null,

    @field:SerializedName("USDVND")
    var uSDVND: Double? = null,

    @field:SerializedName("USDXAU")
    var uSDXAU: Double? = null,

    @field:SerializedName("USDZMW")
    var uSDZMW: Double? = null,

    @field:SerializedName("USDLRD")
    var uSDLRD: Double? = null,

    @field:SerializedName("USDXAG")
    var uSDXAG: Double? = null,

    @field:SerializedName("USDALL")
    var uSDALL: Double? = null,

    @field:SerializedName("USDCHF")
    var uSDCHF: Double? = null,

    @field:SerializedName("USDHRK")
    var uSDHRK: Double? = null,

    @field:SerializedName("USDDJF")
    var uSDDJF: Double? = null,

    @field:SerializedName("USDXAF")
    var uSDXAF: Double? = null,

    @field:SerializedName("USDKGS")
    var uSDKGS: Double? = null,

    @field:SerializedName("USDSOS")
    var uSDSOS: Double? = null,

    @field:SerializedName("USDVEF")
    var uSDVEF: Double? = null,

    @field:SerializedName("USDVUV")
    var uSDVUV: Double? = null,

    @field:SerializedName("USDLAK")
    var uSDLAK: Double? = null,

    @field:SerializedName("USDBND")
    var uSDBND: Double? = null,

    @field:SerializedName("USDZMK")
    var uSDZMK: Double? = null,

    @field:SerializedName("USDETB")
    var uSDETB: Double? = null,

    @field:SerializedName("USDJEP")
    var uSDJEP: Double? = null,

    @field:SerializedName("USDDZD")
    var uSDDZD: Double? = null,

    @field:SerializedName("USDPAB")
    var uSDPAB: Double? = null,

    @field:SerializedName("USDGGP")
    var uSDGGP: Double? = null,

    @field:SerializedName("USDSGD")
    var uSDSGD: Double? = null,

    @field:SerializedName("USDMKD")
    var uSDMKD: Double? = null,

    @field:SerializedName("USDUSD")
    var uSDUSD: Int? = null
) {
    fun getMembers(): List<FxItem> {
        val list = mutableListOf<FxItem>()
        val fields = this::class.java.declaredFields
        for (member in fields) {
            val sName = member.getAnnotation(SerializedName::class.java).value
//            member.isAccessible = true
            Log.d("getMembers sName", sName)
            Log.d("getMembers", "member name ${member.name}")
            Log.d("getMembers", member.toJsonString())
            Log.d("getMembers", member.get(this)?.toJsonString() + "")
            val fieldValue = member.get(this)
            var value = 0.0
            if (fieldValue is Int) {
                value = fieldValue.toDouble()
            } else if (fieldValue is Double) {
                value = fieldValue
            }
            val fxitem = FxItem(sName,value)
            list.add(fxitem)
        }
        return list
    }
}