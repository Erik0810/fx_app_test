package com.example.myapplication.vm

import android.app.Application
import android.util.Log
import android.webkit.WebStorage
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.myapplication.BuildConfig
import com.example.myapplication.extestion.toJsonString
import com.example.myapplication.model.FXResponse
import com.example.myapplication.model.FxItem
import com.example.myapplication.restful.ApiManger
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

class FXViewModel(application: Application) : AndroidViewModel(application) {

    val TAG = FXViewModel::class.java.simpleName
    var scheduler: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
    val fxApi = ApiManger.instance.getFxApi()
    val fxResponse = MutableLiveData<FXResponse?>()
    val originValue = MutableLiveData<List<FxItem>>()
    val liveValue = MutableLiveData<List<FxItem>?>()

    private fun fetchFxList() {
        fxApi.getLiveFXList().enqueue(object : Callback<FXResponse?> {
            override fun onResponse(call: Call<FXResponse?>, response: Response<FXResponse?>) {
                if (response.isSuccessful) {
                    val fxListResponse = response.body()
                    fxResponse.value = fxListResponse
                    val fxList = fxListResponse?.quotes?.getMembers()
                    if (originValue.value == null) {
                        originValue.value = fxList
                    }
                    liveValue.value = updateChange(fxList, originValue.value)
                    Log.d(TAG, "fxList=> ${fxList?.toJsonString()}")
                } else {
                    Log.d(TAG, "fetchFxList Fail")
                }
            }

            override fun onFailure(call: Call<FXResponse?>, t: Throwable) {
                Log.d(TAG, "an error @ fetchFxList : ${t.localizedMessage}")
            }

        })
    }

    fun scheduleFetch() {
        scheduler.scheduleAtFixedRate({
            fetchFxList()
        }, 0, BuildConfig.REFRESH_ITERNVAL, TimeUnit.SECONDS)
    }

    fun stopSchedule(){
        scheduler.shutdownNow()
    }

    private fun updateChange(newValue: List<FxItem>?, origin: List<FxItem>?): List<FxItem>? {
        Log.d(TAG, "updateChange")
        newValue?.forEach { item ->
            val name = item.name
            Log.d(TAG, "name => $name")
            val oldValue = origin?.filter { it.name == name }
            Log.d(TAG, "oldValue=> ${oldValue?.toJsonString()}")
            val change = changeValue(oldValue?.get(0)?.value, item.value)
            Log.d(TAG, "change=> $change")
            item.change = change
            Log.d(TAG, "item=> ${oldValue?.toJsonString()}")
        }
        return newValue
    }

    private fun changeValue(oldVale:Double?, newValue: Double?): Double {
        if (oldVale != null) {
            return (oldVale - newValue!!) /oldVale * 100
        }
        return 0.0
    }
}