package com.example.myapplication.ui.fx

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.R
import com.example.myapplication.extestion.roundUpTo
import com.example.myapplication.model.FxItem
import com.example.myapplication.vm.FXViewModel
import kotlinx.android.synthetic.main.fragment_fx.*
import kotlinx.android.synthetic.main.list_item_fx.*

class FxFragment : Fragment() {

    val TAG = FxFragment::class.java.simpleName
    var viewModel: FXViewModel? = null
    var adapter: FXAdapter? = null
    var sortByUp = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fx, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel?.stopSchedule()
    }

    private fun bindViewModel() {
        viewModel = ViewModelProvider(this).get(FXViewModel::class.java)
        viewModel!!.originValue.observe(viewLifecycleOwner, {
            Log.d(TAG, "originValue change")
        })
        viewModel!!.liveValue.observe(viewLifecycleOwner, {
            Log.d(TAG, "liveValue update")
            if (null != it) {
                Log.d(TAG, "liveValue update not null")
                adapter?.fxList = it
                adapter?.sortDataByChange(sortByUp)
                tv_balance.text = "$${(getBalacne(it))}"
                tv_equity.text = "$${getEquity(it).roundUpTo(2)}"
            }
        })
        adapter = FXAdapter(arrayListOf())
        recycle_view.adapter = adapter
        viewModel?.scheduleFetch()
        textView2.setOnClickListener {
            sortByUp = !sortByUp
            adapter?.sortDataByChange(sortByUp)
        }
    }

    private fun getBalacne(items: List<FxItem>): Double {
        var balance = 0.0
        items.forEach {
            balance += it.balance
        }
        return balance
    }

    private fun getEquity(items: List<FxItem>): Double {
        var equity = 0.0
        items.forEach {
            equity += it.balance * (1 + it.change)
        }
        return equity
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FxFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() = FxFragment()
    }
}