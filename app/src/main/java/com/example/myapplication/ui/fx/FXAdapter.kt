package com.example.myapplication.ui.fx

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.extestion.roundUpTo
import com.example.myapplication.model.FxItem

class FXAdapter(var fxList: List<FxItem>) : RecyclerView.Adapter<FxItemViewHolder>() {

    val TAG = FXAdapter::class.java.simpleName
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FxItemViewHolder {
        val context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_fx, parent, false)
        return FxItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: FxItemViewHolder, position: Int) {
        val itemToBind = fxList[position]
        val colorCode = if(itemToBind.change > 0 ) Color.GREEN else Color.RED
        holder.symbol.text = itemToBind.name
        holder.change.text = "${itemToBind.change?.roundUpTo(2)}%"
        holder.change.setTextColor(colorCode)
        holder.sell.text = itemToBind.sell?.roundUpTo(4)
        holder.buy.text = itemToBind.buy?.roundUpTo(4)
    }

    override fun getItemCount(): Int {
        return fxList.size
    }

    fun sortDataByChange(up: Boolean) {
        Log.d(TAG, "up => $up")
        fxList = if (up) {
            fxList.sortedBy { it.change }
        } else {
            fxList.sortedByDescending { it.change }
        }
        notifyDataSetChanged()
    }
}

class FxItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val symbol = itemView.findViewById<TextView>(R.id.tv_symbol)
    val change = itemView.findViewById<TextView>(R.id.tv_change)
    val sell = itemView.findViewById<TextView>(R.id.tv_sell)
    val buy = itemView.findViewById<TextView>(R.id.tv_buy)
}