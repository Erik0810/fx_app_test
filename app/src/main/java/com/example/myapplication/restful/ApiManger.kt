package com.example.myapplication.restful

import com.example.myapplication.BuildConfig
import com.example.myapplication.restful.api.FxApi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ApiManger {

    companion object{
        val instance: ApiManger = ApiManger()
    }

    private val okHttpClient: OkHttpClient

    init {
        val client = RestfulClient()
        okHttpClient = client.okHttpClient
    }

    fun getFxApi(): FxApi {
        val builder = Retrofit.Builder()
        builder.baseUrl(BuildConfig.BASE_API_URL)
        builder.addConverterFactory(GsonConverterFactory.create())
        builder.client(okHttpClient)
        val retrofit = builder.build()
        return retrofit.create(FxApi::class.java)
    }
}