package com.example.myapplication.restful.api

import com.example.myapplication.BuildConfig
import com.example.myapplication.model.FXResponse
import retrofit2.Call
import retrofit2.http.GET

interface FxApi {
    @GET("/live?access_key=${BuildConfig.FX_API_KEY}")
    fun getLiveFXList(): Call<FXResponse?>
}