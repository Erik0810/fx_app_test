package com.example.myapplication.restful

import com.example.myapplication.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit


class RestfulClient {

    val CONNECT_TIMEOUT = 60
    val WRITE_TIMEOUT = 60
    val READ_TIMEOUT = 60

    val okHttpClient: OkHttpClient

    init {
        okHttpClient = initHttpClient()
    }

    private fun initHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val okHttpClient = OkHttpClient.Builder()
            .connectTimeout(CONNECT_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .addInterceptor(Interceptor { chain ->
                val original: Request = chain.request()
                val request: Request = original.newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .method(original.method, original.body).build()
                chain.proceed(request)
            })
        if (BuildConfig.DEBUG) {
            okHttpClient.addInterceptor(interceptor)
        }
        return okHttpClient.build()

    }
}