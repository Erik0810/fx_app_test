package com.example.myapplication.extestion

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import java.lang.Exception
import java.math.RoundingMode

fun Any.toJsonString(): String {
    return GsonBuilder().serializeSpecialFloatingPointValues().create().toJson(this)
}

fun Double.roundUpTo(range: Int): String {
//    return try {
//        this.toBigDecimal().setScale(range, RoundingMode.UP).toDouble().toString()
//    }catch (e:Exception){
//        return ""
//    }
    return "%.${range}f".format(this)
}